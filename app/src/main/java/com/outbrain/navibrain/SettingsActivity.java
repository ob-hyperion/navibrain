package com.outbrain.navibrain;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;


public class SettingsActivity extends MainActivity implements Switch.OnCheckedChangeListener, Button.OnClickListener {

    Button enableLocationButton;
    EditText editText;
    Switch enableSwitch;
    Button saveSettings;

    public static final String PREFS_NAME = "MyPreferencesFile";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        EditText name = (EditText)findViewById(R.id.users_name);

        //Set activity title
        setTitle("Settings");

        saveSettings = (Button)findViewById(R.id.settings_button);
        saveSettings.setOnClickListener(this);


        enableSwitch = (Switch) findViewById(R.id.switch2);
        enableSwitch.setOnCheckedChangeListener(this);

        editText = (EditText) findViewById(R.id.users_name);
        editText.setText(getUserName());

        enableSwitch.setChecked(getEnableScanFlag());


    }

    private void buttonClick() {
        //Change enableLocationFlag
        setEnableScanFlag(!enableLocationFlag);
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //Change enableLocationFlag
        setEnableScanFlag(isChecked);
    }

    @Override
    public void onClick(View v) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME , 0);
        SharedPreferences.Editor editor = settings.edit();
        //Set user name
        setUserName(editText.getText().toString());
        updateName();
        //Set user name in shared preferences
        editor.putString("name", getUserName());
        editor.putBoolean("enableFlag", getEnableScanFlag());
        editor.putBoolean("updateName", true);

        editor.commit();

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
}
