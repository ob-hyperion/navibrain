package com.outbrain.navibrain.other;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by nvelleman on 1/7/15.
 */
public class WifiScanReceiver extends BroadcastReceiver {
    public static final String PREFS_NAME = "MyPreferencesFile";


    public WifiScanReceiver() {
    }

    @SuppressLint("UseValueOf")
    public void onReceive(Context c, Intent intent) {
        Log.d("deb", "Got Scan Result");
        WifiManager wifiManager = (WifiManager) c.getSystemService(Context.WIFI_SERVICE);
        List<ScanResult> scanResult = wifiManager.getScanResults();

        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME, 0);
        boolean enabled = settings.getBoolean("enableFlag", true);

        Log.d("deb", "enabled = " + enabled);

        if (enabled) {
            Intent i = new Intent(c, TransmitterService.class);
            try {
                i.putExtra("data", toJSon(c, scanResult));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            c.startService(i);
            Log.d("deb", "Start Scan ...");
            wifiManager.startScan();
        }
    }

    public String toJSon(Context c, List<ScanResult> scanResult) throws JSONException {

        SharedPreferences settings = c.getSharedPreferences(PREFS_NAME, 0);
        Log.d("deb", settings.getString("name", ""));

        JSONArray rssiArr = new JSONArray();

        for (ScanResult singleScan : scanResult) {
            JSONObject singleScanJsonObj = new JSONObject();
            singleScanJsonObj.put("bssid", singleScan.BSSID);
            singleScanJsonObj.put("ssid", singleScan.SSID);
            singleScanJsonObj.put("level", singleScan.level);
            rssiArr.put(singleScanJsonObj);
        }

        JSONObject jsonResult = new JSONObject();
        jsonResult.put("user", settings.getString("name", ""));
        jsonResult.put("rssi", rssiArr);

        JSONObject jsonResultWrapper = new JSONObject();
        jsonResultWrapper.put("locRequest", jsonResult);

        return jsonResultWrapper.toString();
    }

}
