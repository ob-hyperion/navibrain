package com.outbrain.navibrain.other;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

/**
 * Created by nvelleman on 1/6/15.
 */
public class TransmitterService extends IntentService {


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */

    public TransmitterService() {
        super("TransmitterService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {

        try {
            post(intent.getStringExtra("data"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static HttpResponse post(String json) throws IOException {

        DefaultHttpClient client = new DefaultHttpClient();

//        HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
//        SchemeRegistry registry = new SchemeRegistry();
//        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
//        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
//        registry.register(new Scheme("http", socketFactory, 443));
//        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
//        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());
//
//  // Set verifier
//        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);


        final String url = "http://54.172.13.31:8080/api/location";
        HttpPost httpPost = new HttpPost(url);
        StringEntity se = new StringEntity(json);
        se.setContentType("application/json;charset=UTF-8");
        httpPost.setEntity(se);

        HttpResponse httpresponse = client.execute(httpPost);
        Log.d("deb" , httpresponse.toString());
        return httpresponse;

    }

}
