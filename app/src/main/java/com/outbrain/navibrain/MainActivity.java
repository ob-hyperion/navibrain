package com.outbrain.navibrain;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.outbrain.navibrain.other.TransmitterService;

import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    private Button button2;
    private Button button3;
    private Button button5;
    private static boolean updateName;
    protected static boolean enableLocationFlag = true;
    protected static String userName = "My name";

    public static final String PREFS_NAME = "MyPreferencesFile";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME , 0);
        updateName = settings.getBoolean("updateName", false);
        setUserName(settings.getString("name", getBluetoothName()));
        setEnableScanFlag(settings.getBoolean("enableFlag", true));


        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(this);

        button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(this);

        button5 = (Button) findViewById(R.id.button5);
        button5.setOnClickListener(this);

        firstTimeScan();
    }

   private void firstTimeScan() {
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        Log.d("deb", "First Scan ...");
        wifiManager.startScan();
    }


    public String toJSon(List<ScanResult> scanResult) throws JSONException {

        JSONArray rssiArr = new JSONArray();

        for (ScanResult singleScan : scanResult) {
            JSONObject singleScanJsonObj = new JSONObject();
            singleScanJsonObj.put("bssid", singleScan.BSSID);
            singleScanJsonObj.put("ssid", singleScan.SSID);
            singleScanJsonObj.put("level", singleScan.level);
            rssiArr.put(singleScanJsonObj);
        }

        JSONObject jsonResult = new JSONObject();
        jsonResult.put("user", getUserName());
        jsonResult.put("rssi", rssiArr);

        JSONObject jsonResultWrapper = new JSONObject();
        jsonResultWrapper.put("locRequest", jsonResult);

        return jsonResultWrapper.toString();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void go2Activity(final Class<? extends Activity> ActivityToOpen) {

        Intent myIntent = new Intent(MainActivity.this, ActivityToOpen);
        MainActivity.this.startActivity(myIntent);
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button2:
                go2Activity(ViewMapActivity.class);
                break;


            case R.id.button3:
                go2Activity(AboutUsActivity.class);
                break;


            case R.id.button5:
                go2Activity(SettingsActivity.class);
                break;
        }
    }

    protected void setEnableScanFlag(boolean value) {
        enableLocationFlag = value;
        if (enableLocationFlag) {
            firstTimeScan();
        }
    }

    protected boolean getEnableScanFlag() {

        return enableLocationFlag;
    }

    protected void setUserName(String userName) {

        this.userName = userName;
    }

    protected String getUserName() {

        return userName;
    }

    protected void updateName() {

        updateName = true;
    }

    public String getBluetoothName() {
        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        String deviceName = null;
        try {
            deviceName = myDevice.getName();
        } catch (Exception e) {
            deviceName = "";
        }

        if(!deviceName.isEmpty() && !updateName)
        {
            setUserName(deviceName);
            //Set Device name in share Preferences
            SharedPreferences settings = getSharedPreferences(PREFS_NAME , 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("name", getUserName());
            editor.commit();
        }
        return deviceName;
   }
}

